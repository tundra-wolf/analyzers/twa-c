use tw_source::reader::messages::ReaderMessage;
use twa_cpp::messages::PreprocessorMessage;

/// Messages for the C parser/analyzer
#[derive(Debug)]
pub enum Message {
    Analyzer(AnalyzerMessage),
    Lexer(LexerMessage),
    Parser(ParserMessage),
    Preprocessor(PreprocessorMessage),
    Reader(ReaderMessage),
}

/// Analyzer messages
#[derive(Debug)]
pub enum AnalyzerMessage {}

/// Lexer messages
#[derive(Debug)]
pub enum LexerMessage {}

/// Parser messages
#[derive(Debug)]
pub enum ParserMessage {}
