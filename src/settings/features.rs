use clap::ValueEnum;
use twa_cpp::settings::features::CppFeature;

/// Supported C Features
#[derive(Clone, Debug, ValueEnum)]
pub enum CFeature {
    /// Preprocessor warning
    CppWarning,

    /// C dynamic arrays
    DynamicArray,

    /// 128-bit floating points (float128)
    Float128,

    /// 128-bit integers (int128)
    Int128,

    /// 256-bit integers (int256)
    Int256,
}

impl Into<CppFeature> for CFeature {
    fn into(self) -> CppFeature {
        match self {
            Self::CppWarning => CppFeature::Warning,
            _ => CppFeature::None,
        }
    }
}
