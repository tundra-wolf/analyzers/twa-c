use clap::ValueEnum;
use twa_cpp::settings::language::{CppExtension, CppStandard};

/// Supported C Extensions
#[derive(Clone, Debug, ValueEnum)]
pub enum CExtension {
    /// No extensions
    Standard,

    /// Clang extensions
    Clang,

    /// GNU extensions
    Gnu,

    /// Microsoft extensions
    Ms,

    /// Objective-C
    ObjectiveC,
}

impl Default for CExtension {
    fn default() -> Self {
        Self::Gnu
    }
}

impl Into<CppExtension> for CExtension {
    fn into(self) -> CppExtension {
        match self {
            Self::Standard => CppExtension::StandardC,
            Self::Clang => CppExtension::Clang,
            Self::Gnu => CppExtension::Gnu,
            Self::Ms => CppExtension::MsC,
            Self::ObjectiveC => CppExtension::ObjectiveC,
        }
    }
}

/// Supported C Standards.
#[derive(Clone, Debug, ValueEnum)]
pub enum CStandard {
    /// Kernighan and Ritchie
    KAndR,

    /// C89
    C89,

    /// C99
    C99,

    /// C11
    C11,

    /// C18
    C18,

    /// C as per the current draft standard
    C2y,
}

impl Default for CStandard {
    fn default() -> Self {
        Self::C18
    }
}

impl Into<CppStandard> for CStandard {
    fn into(self) -> CppStandard {
        match self {
            Self::KAndR => CppStandard::KAndR,
            Self::C89 => CppStandard::C89,
            Self::C99 => CppStandard::C99,
            Self::C11 => CppStandard::C11,
            Self::C18 => CppStandard::C18,
            Self::C2y => CppStandard::C2y,
        }
    }
}
