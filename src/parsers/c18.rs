use tw_analyzer::parsers::{ParseResult, Parser};
use tw_errors::{InternalError, TWError};

/// C18 parser
#[derive(Debug)]
pub struct C18Parser {}

impl Parser for C18Parser {
    fn parse(&mut self) -> Result<Box<dyn ParseResult>, TWError> {
        Err(TWError::InternalError(InternalError::NotImplemented(String::from(
            "C18Parser::parse",
        ))))
    }
}
