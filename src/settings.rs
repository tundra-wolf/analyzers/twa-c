pub mod features;
pub mod language;

use std::path::PathBuf;

use clap::Parser;

use crate::settings::features::CFeature;
use crate::settings::language::{CExtension, CStandard};

/// C analyzer settings
#[derive(Clone, Debug, Parser)]
pub struct CSettings {
    /// C Standards
    #[arg(long = "std", short = 's', default_value_t = CStandard::C18, value_enum)]
    standard: CStandard,

    /// C Extensions
    #[arg(long = "ext", short = 'E', default_value_t = CExtension::Gnu, value_enum)]
    extension: CExtension,

    /// C Features
    #[arg(long = "feature", short = 'f', value_enum)]
    features: Vec<CFeature>,

    /// Source file
    source_file: PathBuf,

    /// Project ID
    project_id: Option<String>,
}
