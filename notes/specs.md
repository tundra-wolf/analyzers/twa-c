# C Analyzer - Specs

## C Standards

* The latest ISO standard for the [C Programming Language](https://www.iso.org/standard/74528.html) is available from ISO.

Earlier standards can in general be found on the internet, either as free copies of the standard or as latest draft before certification. See [WG14 - C Programming Lamnguage](https://www.open-std.org/jtc1/sc22/wg14/www/projects.html) for details.

## Clang C Extensions

Clang broadly support the GNU C extensions, but adds some more or modifies some of the behaviour. The details are [here](https://clang.llvm.org/docs/LanguageExtensions.html#final-macros).

## GNU C Extensions

The GNU GCC documentation describes the [GNU C Extensions](https://gcc.gnu.org/onlinedocs/gcc/C-Extensions.html) to the C programming language.

## Microsoft C Extensions

Microsoft adds some extensions to their C (and C++) compilers. They are defined [here](https://learn.microsoft.com/en-us/cpp/build/reference/microsoft-extensions-to-c-and-cpp?view=msvc-170).

## Objective C Language

The Objective C language is used in older MacOS project.

* Information can be found in the documentation archives on the [Apple website](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html#//apple_ref/doc/uid/TP40011210-CH1-SW1).

* The grammar and language details are [here](http://andrewd.ces.clemson.edu/courses/cpsc102/notes/ObjC.pdf).

* The Objective-C Runtime is detailed [here](https://developer.apple.com/documentation/objectivec).
